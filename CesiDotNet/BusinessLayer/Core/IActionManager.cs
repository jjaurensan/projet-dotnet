﻿using Common.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Core
{
    public interface IActionManager<T>
    {
        Task Delete(int id);

        Task<T> GetById(int id);

        Task<List<T>> GetAll();

        Task<T> Add(BaseEntity baseEntity);

        Task<T> Update(int id, BaseEntity baseEntity); // on donne id de entity qu'on veut mettre à jour
    }
}
