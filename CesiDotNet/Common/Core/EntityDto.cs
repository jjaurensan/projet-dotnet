﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Core
{
    public class Entitydto
    {
        public Entitydto()
        {
            CreatedOn = DateTime.Now;
            UpdatedOn = DateTime.Now;
        }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}
